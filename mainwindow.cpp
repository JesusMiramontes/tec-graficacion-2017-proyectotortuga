#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QPixmap>
#include <QPainter>
#include <QBrush>
#include <QPalette>
#include <QImage>
#include <QPoint>
#include "tortuga.h"
#include <QtMath>
#include <math.h>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    pix = new QPixmap(ui->drawing_area->width(), ui->drawing_area->height()); //Tamaño canvas
    pix->fill(Qt::transparent); //Fondo transparente
    paint = new QPainter(pix);
    t = new Tortuga(ui->drawing_area->width()/2, ui->drawing_area->height()/2);
    dibujarLinea(t->posicion_actual,t->posicion_actual);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::dibujarLinea(int length)
{
    qreal new_x, new_y;

    float fradians = t->angle;

    qreal radians = (qreal)fradians;
    new_x = (qreal)(t->posicion_actual->x() + length * qCos(radians));
    new_y = (qreal)(t->posicion_actual->y() + length * qSin(radians));

    QPointF* new_point = new QPointF(new_x, new_y);
    dibujarLinea(t->posicion_actual,new_point);
    t->posicion_actual = new_point;
}

void MainWindow::dibujarLinea(QPointF *p1, QPointF *p2, int width)
{
    QPen pen;  // creates a default pen

    pen.setStyle(Qt::SolidLine); //Estilo de linea
    pen.setWidth(width); //Ancho de linea
    pen.setBrush(Qt::black); //Color de lina
    pen.setCapStyle(Qt::SquareCap); //Forma de extremos de lina (cuadrado, redondeado, etc)
    //pen.setJoinStyle(Qt::RoundJoin);

    paint->setPen(pen); //Color separador
    paint->drawLine(*p1, *p2);

    ui->drawing_area->setPixmap(*pix);
}
