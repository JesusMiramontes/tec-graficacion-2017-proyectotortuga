#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "tortuga.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    QPixmap *pix;
    QPainter* paint;
    Tortuga *t;
    ~MainWindow();

private slots:
    void on_pushButton_clicked();
    void dibujarLinea(int length=3);
    void dibujarLinea(QPointF *p1, QPointF *p2, int width=3);
private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
