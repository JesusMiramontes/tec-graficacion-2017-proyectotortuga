#ifndef TORTUGA_H
#define TORTUGA_H
#include <QPoint>

class Tortuga
{
public:
    Tortuga();
    Tortuga(int x, int y);
    QPointF* posicion_actual;
    float angle;
    void setAngle(int a);
};

#endif // TORTUGA_H
